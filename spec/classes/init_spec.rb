require 'spec_helper'
describe 'no_puppet_inst' do

  context 'with defaults for all parameters' do
    it { should contain_class('no_puppet_inst') }
  end
end
